﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfGalgjeUitbreiding1WPL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string teRadenWoord = string.Empty;
        private string gokWoord = string.Empty;
        private StringBuilder SBjuisteLetters = new StringBuilder();
        private StringBuilder SBfouteLetters = new StringBuilder();
        private string gemaskeerdWoord;
        
       
        private short aantalLevens = 10;
        private short aantalBeurten = 1;
        /*uitbreiding*/
        private DispatcherTimer timer = new DispatcherTimer();
        private int teller = 10;
        
        public MainWindow()
        {

            InitializeComponent();
            TxtIngaveveld.Focus();
            /*
            Image afbGalg = new Image();
            afbGalg.Source = new BitmapImage(new Uri(@"/Images/Galg1.jpg", UriKind.RelativeOrAbsolute));
            afbGalg.Stretch = Stretch.Fill;
            PicAfbeeldingGalg.Source = afbGalg;
            */

        }
        /*Events*/
        private void BtnVerbergWoord_Click(object sender, RoutedEventArgs e)
        {
            teRadenWoord = TxtIngaveveld.Text;
            


            /*het woord verbergen en tonen in masking tekstblok*/
            gemaskeerdWoord = teRadenWoord.Substring(teRadenWoord.Length).PadLeft(teRadenWoord.Length, '*');
            TxtMasking.Text = gemaskeerdWoord;
            TxtIngaveveld.Clear();
            TxtIngaveveld.Focus();
            BtnVerbergWoord.Visibility = Visibility.Hidden;
            BtnRaad.IsEnabled = true;
            UpdateInfoveld();
            /*uitbreiding timer*/
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
            /*afbeelding laten tonen*/
            
            PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg1.jpg", UriKind.RelativeOrAbsolute));
            PicAfbeeldingGalg.Stretch = Stretch.Fill;
             
        

    }

        private void timer_Tick(object sender, EventArgs e)
        {
            TxtTimer.Text = teller.ToString();
            if (teller.Equals(11))
            {
                TxtTimer.Visibility = Visibility.Hidden;
            }
            else
            {
                TxtTimer.Visibility = Visibility.Visible;
            }
            teller--;
            GridWindow.Background = Brushes.LightSlateGray;
            TxtTimer.Background = (teller.Equals(1) || teller.Equals(0)) ? TxtTimer.Background = Brushes.IndianRed : TxtTimer.Background = Brushes.LightGray;
            if (teller.Equals(-1))
            {
                timer.Stop();
                aantalBeurten++;
                aantalLevens--;
                ToonJuistAfbeelding();
                UpdateInfoveld();
                GridWindow.Background = Brushes.IndianRed;
                teller = 11;
                timer.Start();
            }
        }

        private void BtnRaad_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            gokWoord = TxtIngaveveld.Text;

            if (teller.Equals(0))
            {
                aantalBeurten++;
                aantalLevens--;
                ToonJuistAfbeelding();
                TxtIngaveveld.Clear();
                TxtIngaveveld.Focus();
                UpdateInfoveld();
            }
            if (gokWoord.Length.Equals(1))
            {
                
                    TestOfLetterJuistOfFoutIs();
                    // masking updaten.

            }
            aantalBeurten++;
            aantalLevens--;
            ToonJuistAfbeelding();
            TxtIngaveveld.Clear();
            TxtIngaveveld.Focus();
            UpdateInfoveld();
            
            
            teller = 10;
            timer.Start();
            /*testen of je nog levens over hebt.*/
            if (aantalLevens.Equals(0))
            {
                // code met verliezersboodschap
                TxtInfoveld.Text = $"Helaas u heeft het woord '{teRadenWoord}' niet kunnen raden. \r\n " +
                    "Je bent opgehangen !";
                TxtInfoveld.Background = Brushes.Red;
                PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg11.jpg", UriKind.RelativeOrAbsolute));
                timer.Stop();
                DeactiveerKnoppen();

            }
            if (gokWoord.Length > 1)
            {
                /*testen of ingave gelijk is aan oplossing*/
                if (gokWoord.Equals(teRadenWoord))
                {
                    // winnaarsboodschap
                    TxtInfoveld.Text = $"Proficiat u heeft {teRadenWoord} geraden in '{aantalBeurten}'Poging/pogingen. ";
                    TxtInfoveld.Background = Brushes.LightGreen;
                    timer.Stop();
                    DeactiveerKnoppen();
                }


            }


        }

        private void BtnNieuwspel_Click(object sender, RoutedEventArgs e)
        {
            TxtInfoveld.Text = "Geef een geheim woord in";
            TxtIngaveveld.Text = String.Empty;
            BtnVerbergWoord.IsEnabled = true;
            BtnVerbergWoord.Visibility = Visibility.Visible;
            BtnRaad.IsEnabled = false;
            SBjuisteLetters.Clear();
            SBfouteLetters.Clear();
            TxtFouteLetters.Text = "Fout geraden letters";
            TxtJuisteLetters.Text = "Juist geraden letters";
            TxtInfoveld.Background = new SolidColorBrush(Colors.LightGray);
            TxtTimer.Text = string.Empty;
            TxtMasking.Text = string.Empty;
            aantalLevens = 10;
            aantalBeurten = 1;
            PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg1.jpg", UriKind.RelativeOrAbsolute));
            PicAfbeeldingGalg.Stretch = Stretch.Fill;
        }

        private void BtnNieuwspel_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        /* methoden*/
        private void ToonJuistAfbeelding()
        {
            switch (aantalBeurten)
            {
                case 1:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg1.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 2:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg2.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 3:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg3.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 4:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg4.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 5:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg5.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 6:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg6.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 7:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg8.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 8:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg9.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 9:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg10.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                case 10:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg11.jpg", UriKind.RelativeOrAbsolute));
                    PicAfbeeldingGalg.Stretch = Stretch.Fill;
                    break;
                default:
                    PicAfbeeldingGalg.Source = new BitmapImage(new Uri(@"/Images/Galg1.jpg", UriKind.RelativeOrAbsolute));
                    break;
            }
        }
        private void DeactiveerKnoppen()
        {

            BtnRaad.IsEnabled = false;
            TxtIngaveveld.IsEnabled = false;
            KnopA.IsEnabled = false;
            KnopB.IsEnabled = false;
            KnopC.IsEnabled = false;
            KnopD.IsEnabled = false;
            KnopE.IsEnabled = false;
            KnopF.IsEnabled = false;
            KnopG.IsEnabled = false;
            KnopH.IsEnabled = false;
            KnopI.IsEnabled = false;
            KnopJ.IsEnabled = false;
            KnopK.IsEnabled = false;
            KnopL.IsEnabled = false;
            KnopM.IsEnabled = false;
            KnopN.IsEnabled = false;
            KnopO.IsEnabled = false;
            KnopP.IsEnabled = false;
            KnopQ.IsEnabled = false;
            KnopR.IsEnabled = false;
            KnopS.IsEnabled = false;
            KnopT.IsEnabled = false;
            KnopU.IsEnabled = false;
            KnopV.IsEnabled = false;
            KnopW.IsEnabled = false;
            KnopX.IsEnabled = false;
            KnopY.IsEnabled = false;
            KnopZ.IsEnabled = false;
        }

        private void TestOfLetterJuistOfFoutIs()
        {
            if (teRadenWoord.Contains(gokWoord))
            {
                //code als letter het  te raden woord bevat 
                TxtJuisteLetters.Clear();
                TxtInfoveld.Background = Brushes.LightGreen;
                SBjuisteLetters.Append(gokWoord);
                TxtJuisteLetters.Text += SBjuisteLetters.ToString();
                //foute code
                // gemaskeerdWoord.Replace('*',Convert.ToChar(teRadenWoord));
                int plaatsVanLetterInWoord = teRadenWoord.IndexOf(gokWoord);
              

              
                // HIERZO VERDER GAAN MET FUNCTIES INDEXOF() EN SUBSTRING() OM MASKING TE UPDATEN 
                 
                for (int i = 0; i <= teRadenWoord.Length; i++)
                {

                    if(plaatsVanLetterInWoord.Equals(i))
                    {

                        gemaskeerdWoord.Remove(i, 1);
                        gemaskeerdWoord.Insert(i, gokWoord);
                        TxtMasking.Text = gemaskeerdWoord.ToString();

                    }
                    
                }

            }
            else
            {
                // code als letter het te raden woord niet bevat
                TxtFouteLetters.Clear();
                TxtInfoveld.Background = Brushes.Red;
                SBfouteLetters.Append(gokWoord);
                TxtFouteLetters.Text += SBfouteLetters.ToString();


            }
        }

        private void UpdateInfoveld()
        {
            TxtInfoveld.Text = $"Aantal levens: {aantalLevens} \r\n " +
                                $"Poging: {aantalBeurten}";
        }


        /*toetsenbord */
        private void KnopA_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "a";
        }
        private void KnopB_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "b";
        }

        private void KnopC_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "c";
        }

        private void KnopD_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "d";
        }

        private void KnopE_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "e";
        }

        private void KnopF_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "f";
        }

        private void KnopG_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "g";
        }

        private void KnopH_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "h";
        }

        private void KnopI_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "i";
        }

        private void KnopJ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "j";
        }

        private void KnopK_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "k";
        }

        private void KnopL_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "l";
        }

        private void KnopM_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "m";
        }

        private void KnopN_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "n";
        }

        private void KnopO_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "o";
        }

        private void KnopP_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "p";
        }

        private void KnopQ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "q";
        }

        private void KnopR_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "r";
        }

        private void KnopS_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "s";
        }

        private void KnopT_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "t";
        }

        private void KnopU_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "u";
        }

        private void KnopV_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "v";
        }

        private void KnopW_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "w";
        }

        private void KnopX_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "x";
        }

        private void KnopY_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "y";
        }

        private void KnopZ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "z";
        }

        
    }
}

