﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfGalgjeNieuwWPL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /* private variabelen*/
        private string teRadenWoord = string.Empty;
        private string gokWoord = string.Empty;
        private StringBuilder SBjuisteLetters = new StringBuilder();
        private StringBuilder SBfouteLetters = new StringBuilder();
        private short aantalLevens = 10;
        private short aantalBeurten = 1;
        public MainWindow()
        {

            InitializeComponent();
        }
        /*gebeurtenissen na het klikken van verbergwoord knop  */
        private void BtnVerbergWoord_Click(object sender, RoutedEventArgs e)
        {
            teRadenWoord = TxtIngaveveld.Text;
            TxtIngaveveld.Clear();
            TxtIngaveveld.Focus();
            BtnVerbergWoord.Visibility = Visibility.Hidden;
            BtnRaad.IsEnabled = true;
            UpdateInfoveld();
            
           
        }

        
        private void BtnRaad_Click(object sender, RoutedEventArgs e)
        {
          gokWoord = TxtIngaveveld.Text;
        
            if (gokWoord.Length.Equals(1))
            {
                TestOfLetterJuistOfFoutIs(); 
            }

            aantalBeurten++;
            aantalLevens--;
            TxtIngaveveld.Clear();
            TxtIngaveveld.Focus();
            UpdateInfoveld();
            if (gokWoord.Length > 1)
            {
                if (gokWoord.Equals(teRadenWoord))
                {
                    // winnaarsboodschap
                    TxtInfoveld.Text = $"Proficiat u heeft {teRadenWoord} geraden in '{aantalBeurten}'Poging/pogingen. ";
                    TxtInfoveld.Background = Brushes.LightGreen;
                    
                    BtnNieuwspel.Focus();
                    /*knoppen deactiviveren*/
                    DeactiveerKnoppen();
                }
                

            }

            if (aantalLevens.Equals(0))
            {
                // code met verliezersboodschap
                TxtInfoveld.Text = $"Helaas u heeft het woord '{teRadenWoord}' niet kunnen raden. \r\n " +
                    "Je bent opgehangen !";
                TxtInfoveld.Background = Brushes.Red;
                DeactiveerKnoppen();
            }
        }

        private void BtnNieuwspel_Click(object sender, RoutedEventArgs e)
        {
            TxtInfoveld.Text = "Geef een geheim woord in";
            TxtIngaveveld.Text = String.Empty;
            BtnVerbergWoord.IsEnabled = true;
            BtnVerbergWoord.Visibility = Visibility.Visible;
            BtnRaad.IsEnabled = false;
            SBjuisteLetters.Clear();
            SBfouteLetters.Clear();
            TxtFouteLetters.Text = "Fout geraden letters";
            TxtJuisteLetters.Text = "Juist geraden letters";
            aantalLevens = 10;
            aantalBeurten = 1;
        }
        
        private void TestOfLetterJuistOfFoutIs()
        {
            if (teRadenWoord.Contains(gokWoord))
            {
                //code als letter het  te raden woord bevat 
                TxtJuisteLetters.Clear();
                TxtInfoveld.Background = Brushes.LightGreen;
                SBjuisteLetters.Append(gokWoord);
                TxtJuisteLetters.Text += SBjuisteLetters.ToString();




            }
            else
            {
                // code als letter het te raden woord niet bevat
                TxtFouteLetters.Clear();
                TxtInfoveld.Background = Brushes.Red;
                SBfouteLetters.Append(gokWoord);
                TxtFouteLetters.Text += SBfouteLetters.ToString();


            }
        }
       

        private void UpdateInfoveld()
        {
            TxtInfoveld.Text = $"Aantal levens: {aantalLevens} \r\n " +
                                $"Poging: {aantalBeurten}";
        }

        private void DeactiveerKnoppen()
        {
           
            BtnRaad.IsEnabled = false;
            TxtIngaveveld.IsEnabled = false;
            KnopA.IsEnabled = false;
            KnopB.IsEnabled = false;
            KnopC.IsEnabled = false;
            KnopD.IsEnabled = false;
            KnopE.IsEnabled = false;
            KnopF.IsEnabled = false;
            KnopG.IsEnabled = false;
            KnopH.IsEnabled = false;
            KnopI.IsEnabled = false;
            KnopJ.IsEnabled = false;
            KnopK.IsEnabled = false;
            KnopL.IsEnabled = false;
            KnopM.IsEnabled = false;
            KnopN.IsEnabled = false;
            KnopO.IsEnabled = false;
            KnopP.IsEnabled = false;
            KnopQ.IsEnabled = false;
            KnopR.IsEnabled = false;
            KnopS.IsEnabled = false;
            KnopT.IsEnabled = false;
            KnopU.IsEnabled = false;
            KnopV.IsEnabled = false;
            KnopW.IsEnabled = false;
            KnopX.IsEnabled = false;
            KnopY.IsEnabled = false;
            KnopZ.IsEnabled = false;
        }
        /*toetsenbord */
        private void KnopA_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "a";
        }
        private void KnopB_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "b";
        }

        private void KnopC_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "c";
        }

        private void KnopD_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "d";
        }

        private void KnopE_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "e";
        }

        private void KnopF_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "f";
        }

        private void KnopG_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "g";
        }

        private void KnopH_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "h";
        }

        private void KnopI_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "i";
        }

        private void KnopJ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "j";
        }

        private void KnopK_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "k";
        }

        private void KnopL_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "l";
        }

        private void KnopM_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "m";
        }

        private void KnopN_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "n";
        }

        private void KnopO_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "o";
        }

        private void KnopP_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "p";
        }

        private void KnopQ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "q";
        }

        private void KnopR_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "r";
        }

        private void KnopS_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "s";
        }

        private void KnopT_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "t";
        }

        private void KnopU_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "u";
        }

        private void KnopV_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "v";
        }

        private void KnopW_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "w";
        }

        private void KnopX_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "x";
        }

        private void KnopY_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "y";
        }

        private void KnopZ_Click(object sender, RoutedEventArgs e)
        {
            TxtIngaveveld.Text += "z";
        }
    }
 }

